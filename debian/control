Source: golang-github-jedisct1-dlog
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Eric Dorland <eric@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-hashicorp-go-syslog-dev
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/jedisct1/dlog
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jedisct1-dlog
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jedisct1-dlog.git
XS-Go-Import-Path: github.com/jedisct1/dlog
Testsuite: autopkgtest-pkg-go

Package: golang-github-jedisct1-dlog-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         golang-github-hashicorp-go-syslog-dev
Description: Super simple logger for Go
 Go's standard logger is fairly limited. As result, kazilion alternatives
 loggers have been written.
 .
 All of these are wonderful. They can make your logs look colorful and
 pretty, buffer things in complicated ways, format data for ElasticSearch,
 and more.
 .
 Cool, but all I wanted is something super dumb, that just exposes
 log.Info(), log.Error() and a couple other standard levels.
 .
 I don't need a super flexible kitchen sink. Just something super basic
 and trivial to use. I just want it to handle different log levels, and
 be able to write simple logs to stderr, to a local file, to syslog and
 to the Windows event log.
 .
 So, here's one more logging library for Go. The dumbest of them
 all. Enjoy.
